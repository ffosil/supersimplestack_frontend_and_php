<?php
define('ASSET_BASE_PATH', '/static/');


class AssetLoader {

  const ASSET_BASE_PATH = '/static///';
  const MANIFEST_FILE = __DIR__ . '/../static/dist/parcel-manifest.json';

  public static function get($key) {
    $manifest_realpath = realpath(self::MANIFEST_FILE);
    if (is_readable($manifest_realpath) && is_file($manifest_realpath)) {
      $data = json_decode(file_get_contents($manifest_realpath), true);
      if (array_key_exists($key, $data)) {
        return $data[$key];
      } else {
        throw new AssetLoaderException('File ' . $key . ' neexistuje.');
      }
    } else {
      throw new AssetLoaderException('Manifest file does not exist. Please, run: "yarn build" or "yarn watch"');
    }
  }

}

class AssetLoaderException extends \Exception {}
