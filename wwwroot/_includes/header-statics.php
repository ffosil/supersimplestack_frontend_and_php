<?php
  $ROOT_PATH = __DIR__ . '../../';
  require_once($ROOT_PATH . '_libs/asset-loader.php');
?>
<link href="<?php echo AssetLoader::get('app/index.scss'); ?>" type="text/css" rel="stylesheet">
<script type="text/javascript" src="<?php echo AssetLoader::get('app/index.js'); ?>"></script>
