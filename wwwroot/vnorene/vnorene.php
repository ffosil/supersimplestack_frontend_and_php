<?php
  $ROOT_PATH = '../';
  require_once($ROOT_PATH . '_libs/asset-loader.php');
?>
<html>
  <head>
    <title>Vnořená stránka</title>
    <?php require_once($ROOT_PATH . '_includes/header-statics.php'); ?>
  </head>
  <body>
    <h1>
      Vnořená stránka
    </h1>

    <p>
      <a href="/"/>
        Link na HP
      </a>
    </p>

    <hr />
    <img src="<?php echo AssetLoader::get('app/images/test-image.jpg'); ?>" />
  </body>
</html>
